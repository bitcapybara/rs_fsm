use std::{collections::HashSet, sync::Arc, thread};

use crossbeam::channel::{self, SendError};
use parking_lot::Mutex;

use crate::{Event, InternalState, State};

pub struct StateMachine<S: State, E: Event> {
    pub(crate) queue: channel::Sender<Option<E>>,
    pub(crate) join: Option<thread::JoinHandle<()>>,
    pub(crate) machine: Arc<Mutex<InternalStateMachine<S, E>>>,
}

impl<S: State, E: Event> StateMachine<S, E> {
    pub fn fire(&self, event: E) -> Result<(), SendError<Option<E>>> {
        self.queue.send(Some(event))
    }

    pub fn current(&self) -> S {
        self.machine.lock().current()
    }
}

impl<S: State, E: Event> Drop for StateMachine<S, E> {
    fn drop(&mut self) {
        self.queue.send(None).unwrap();
        self.join.take().unwrap().join().unwrap();
    }
}

pub(crate) struct InternalStateMachine<S: State, E: Event> {
    pub(crate) states: Arc<HashSet<InternalState<S, E>>>,
    pub(crate) current: Arc<Mutex<Option<S>>>,
}

impl<S: State, E: Event> InternalStateMachine<S, E> {
    pub fn on_event(&mut self, event: E) {
        // TODO 父子状态，层次状态机
        let c = *self.current.lock();
        if let Some(current) = self.states.get(&InternalState::new(c.to_owned().unwrap())) {
            if let Some(&next_s) = current.next.get(&event) {
                if let Some(action_out) = current.action_out {
                    action_out((*self.current.lock()).unwrap(), event);
                }
                if let Some(next) = self.states.get(&InternalState::new(next_s)) {
                    if let Some(action_in) = next.action_in {
                        action_in(next_s, event);
                    }
                    self.current.lock().replace(next_s);
                }
            }
        }
    }

    pub fn current(&self) -> S {
        (*self.current.lock()).unwrap()
    }
}
