use std::collections::HashSet;
use std::sync::Arc;
use std::thread;

use crossbeam::channel;
use parking_lot::Mutex;

use crate::instance::{InternalStateMachine, StateMachine};
use crate::{Action, Event, InternalState, State};

pub struct StateMachineFactory<S: State, E: Event> {
    states: Arc<HashSet<InternalState<S, E>>>,
}

impl<S: State, E: Event> StateMachineFactory<S, E> {
    pub fn new_instance(self, state: S) -> StateMachine<S, E> {
        let (tx, rx) = channel::unbounded();
        let machine = Arc::new(Mutex::new(InternalStateMachine {
            states: self.states,
            current: Arc::new(Mutex::new(Some(state))),
        }));
        let thread_machine = machine.clone();
        let join = Some(thread::spawn(move || {
            while let Ok(event) = rx.recv() {
                if let Some(event) = event {
                    thread_machine.clone().lock().on_event(event);
                } else {
                    break;
                }
            }
        }));
        StateMachine {
            queue: tx,
            join,
            machine,
        }
    }
}

pub struct StateMachineFactoryBuilder<S: State, E: Event> {
    states: HashSet<InternalState<S, E>>,
}

impl<S: State, E: Event> Default for StateMachineFactoryBuilder<S, E> {
    fn default() -> Self {
        Self::new()
    }
}

impl<S: State, E: Event> StateMachineFactoryBuilder<S, E> {
    pub fn new() -> Self {
        StateMachineFactoryBuilder {
            states: HashSet::new(),
        }
    }

    pub fn transition(self) -> Transition<S, E> {
        Transition {
            states: self.states,
        }
    }

    pub fn state(self, state: S) -> InternalStateBuilder<S, E> {
        InternalStateBuilder {
            state,
            parent: None,
            init_sub: None,
            action_in: None,
            action_out: None,
            states: self.states,
        }
    }

    pub fn finalize(self) -> StateMachineFactory<S, E> {
        // TODO: check
        StateMachineFactory {
            states: Arc::new(self.states),
        }
    }
}

pub struct Transition<S: State, E: Event> {
    states: HashSet<InternalState<S, E>>,
}

impl<S: State, E: Event> Transition<S, E> {
    pub fn from(mut self, state: S) -> TransitionFrom<S, E> {
        let s = InternalState::new(state);
        if !self.states.contains(&s) {
            self.states.insert(s);
        }
        TransitionFrom {
            states: self.states,
            state,
        }
    }

    pub fn finalize(self) -> StateMachineFactoryBuilder<S, E> {
        StateMachineFactoryBuilder {
            states: self.states,
        }
    }
}

pub struct TransitionFrom<S: State, E: Event> {
    state: S,
    states: HashSet<InternalState<S, E>>,
}

impl<S: State, E: Event> TransitionFrom<S, E> {
    pub fn to(self, state: S) -> TransitionTo<S, E> {
        TransitionTo {
            from: self.state,
            to: state,
            states: self.states,
        }
    }
}

pub struct TransitionTo<S: State, E: Event> {
    from: S,
    to: S,
    states: HashSet<InternalState<S, E>>,
}

impl<S: State, E: Event> TransitionTo<S, E> {
    pub fn on(mut self, event: E) -> TransitionOn<S, E> {
        let mut from = InternalState::new(self.from);
        self.states.remove(&from);
        from.next.insert(event, self.to);
        self.states.insert(from);
        TransitionOn {
            states: self.states,
        }
    }
}

pub struct TransitionOn<S: State, E: Event> {
    states: HashSet<InternalState<S, E>>,
}

impl<S: State, E: Event> TransitionOn<S, E> {
    pub fn transition(self) -> Transition<S, E> {
        Transition {
            states: self.states,
        }
    }

    pub fn end_transition(self) -> StateMachineFactoryBuilder<S, E> {
        StateMachineFactoryBuilder {
            states: self.states,
        }
    }
}

pub struct InternalStateBuilder<S: State, E: Event> {
    state: S,
    parent: Option<S>,
    init_sub: Option<S>,
    action_in: Option<Action<S, E>>,
    action_out: Option<Action<S, E>>,
    states: HashSet<InternalState<S, E>>,
}

impl<S: State, E: Event> InternalStateBuilder<S, E> {
    pub fn parent(mut self, parent: S) -> InternalStateBuilder<S, E> {
        self.parent = Some(parent);
        self
    }

    pub fn init_sub(mut self, sub: S) -> InternalStateBuilder<S, E> {
        self.init_sub = Some(sub);
        self
    }

    pub fn action_in(mut self, action: Action<S, E>) -> InternalStateBuilder<S, E> {
        self.action_in = Some(action);
        self
    }

    pub fn action_out(mut self, action: Action<S, E>) -> InternalStateBuilder<S, E> {
        self.action_out = Some(action);
        self
    }

    pub fn end_state(mut self) -> StateMachineFactoryBuilder<S, E> {
        let state = InternalState::new(self.state);
        let mut s = self.states.take(&state).unwrap_or(state);
        s.parent = self.parent;
        s.init_sub = self.init_sub;
        s.action_in = self.action_in;
        s.action_out = self.action_out;
        self.states.insert(s);
        StateMachineFactoryBuilder {
            states: self.states,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::factory::StateMachineFactoryBuilder;
    use crate::{Event, State};

    #[derive(Eq, PartialEq, Hash, Copy, Clone, fsm_derive::State)]
    enum TestState {
        A,
        B,
        C,
    }

    #[derive(Eq, PartialEq, Hash, Copy, Clone, fsm_derive::Event)]
    enum TestEvent {
        X,
        Y,
        Z,
    }

    #[rustfmt::skip]
    #[test]
    fn test_state_machine() {
        let factory = StateMachineFactoryBuilder::<TestState, TestEvent>::new()
            // transitions
            .transition().from(TestState::A).to(TestState::B).on(TestEvent::X)
            .transition().from(TestState::B).to(TestState::C).on(TestEvent::Y)
            .transition().from(TestState::C).to(TestState::A).on(TestEvent::Z)
            .end_transition()
            // states
            .state(TestState::A).parent(TestState::B).init_sub(TestState::C)
            .action_in(|_, _| println!("action in A")).action_out(|_, _| println!("action out A"))
            .end_state()
            // end
            .finalize();
        factory.new_instance(TestState::A);
    }
}
