#![allow(dead_code)]

mod factory;
mod instance;

pub use factory::StateMachineFactoryBuilder;

use std::{
    collections::HashMap,
    hash::{Hash, Hasher},
};

pub trait State: Copy + Eq + PartialEq + Hash + Send + Sync + 'static {}

pub trait Event: Copy + Clone + Eq + PartialEq + Hash + Send + Sync + 'static {}

pub type Action<S, E> = fn(S, E);

#[derive(Eq, Clone)]
pub(crate) struct InternalState<S: State, E: Event> {
    state: S,
    parent: Option<S>,
    init_sub: Option<S>,
    next: HashMap<E, S>,
    action_in: Option<Action<S, E>>,
    action_out: Option<Action<S, E>>,
}

impl<S: State, E: Event> InternalState<S, E> {
    pub fn new(state: S) -> Self {
        Self {
            state,
            parent: None,
            init_sub: None,
            next: HashMap::new(),
            action_in: None,
            action_out: None,
        }
    }
}

impl<S: State, E: Event> PartialEq for InternalState<S, E> {
    fn eq(&self, other: &Self) -> bool {
        self.state == other.state
    }
}

impl<S: State, E: Event> Hash for InternalState<S, E> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.state.hash(state);
    }
}

// impl State for primitive types

macro_rules! state_impl {
    ($state:ty) => {
        impl State for $state {}
    };
}

state_impl!(&'static str);
state_impl!(char);
state_impl!(bool);
state_impl!(u8);
state_impl!(u16);
state_impl!(u32);
state_impl!(u64);
state_impl!(u128);
state_impl!(usize);
state_impl!(i8);
state_impl!(i16);
state_impl!(i32);
state_impl!(i64);
state_impl!(i128);
state_impl!(isize);

// impl Event for primitive types

macro_rules! event_impl {
    ($event:ty) => {
        impl Event for $event {}
    };
}

event_impl!(&'static str);
event_impl!(char);
event_impl!(bool);
event_impl!(u8);
event_impl!(u16);
event_impl!(u32);
event_impl!(u64);
event_impl!(u128);
event_impl!(usize);
event_impl!(i8);
event_impl!(i16);
event_impl!(i32);
event_impl!(i64);
event_impl!(i128);
event_impl!(isize);
