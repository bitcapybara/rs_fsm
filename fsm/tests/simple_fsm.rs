use std::{thread, time::Duration};

#[test]
#[rustfmt::skip]
fn test_state_machine() {
    let factory = rs_fsm::StateMachineFactoryBuilder::<&'static str, &'static str>::new()
    // A -> E1 -> B
    .transition().from("A").to("B").on("E1")
    // B -> E2 -> C
    .transition().from("B").to("C").on("E2")
    // C -> E3 -> A
    .transition().from("C").to("A").on("E3")
    .end_transition()
    // state A
    .state("A")
    .action_in(|_, _| {
        println!("action_in A");
    })
    .action_out(|_, _| {
        println!("action_out A");
    })
    .end_state()
    // state B
    .state("B")
    .action_in(|_, _| {
        println!("action_in B");
    })
    .action_out(|_, _| {
        println!("action_out B");
    })
    .end_state()
    // state C
    .state("C")
    .action_in(|_, _| {
        println!("action_in C");
    })
    .action_out(|_, _| {
        println!("action_out C");
    })
    .end_state()
    // end
    .finalize();

    let fsm_1 = factory.new_instance("A");
    fsm_1.fire("E1").unwrap();
    fsm_1.fire("E2").unwrap();
    fsm_1.fire("E3").unwrap();

    thread::sleep(Duration::from_millis(100));

    assert_eq!(fsm_1.current(), "A");
}
